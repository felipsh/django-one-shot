from django.urls import path
from todos.views import (
    todolist_list,
    todo_list_detail,
    todo_list_create,
    todo_list_update,
)

urlpatterns = [
    path("", todolist_list, name="todolist_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
]
